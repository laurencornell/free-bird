import com.laurenrawrface.freebird.repository.ZoneRepository
import com.laurenrawrface.freebird.service.ZonesService
import com.laurenrawrface.freebird.sprinklers.SprinklerZone
import spock.lang.Specification

class ZonesServiceTest extends Specification{ //All Spock must extend Specification

    ZonesService zonesService = new ZonesService()
    ZoneRepository zoneRepository = new ZoneRepository()
    Map<Integer, SprinklerZone> sprinklerZoneMap = new HashMap<>()
    Map<Integer, SprinklerZone> copySprinklerZoneMap = new HashMap<>()

    def setup(){
        SprinklerZone sprinklerZone1 = new SprinklerZone()
        SprinklerZone sprinklerZone2 = new SprinklerZone()
        SprinklerZone sprinklerZone3 = new SprinklerZone()

        sprinklerZone1.setZoneNumber(1)
        sprinklerZone1.setZoneName("East flower bed")
        sprinklerZoneMap.put(sprinklerZone1.getZoneNumber(), sprinklerZone1)
        copySprinklerZoneMap.put(sprinklerZone1.getZoneNumber(), sprinklerZone1)

        sprinklerZone2.setZoneNumber(2)
        sprinklerZone2.setZoneName("Front north lawn")
        sprinklerZoneMap.put(sprinklerZone2.getZoneNumber(), sprinklerZone2)
        copySprinklerZoneMap.put(sprinklerZone2.getZoneNumber(), sprinklerZone2)

        sprinklerZone3.setZoneNumber(3)
        sprinklerZone3.setZoneName("Back south lawn")
        sprinklerZoneMap.put(sprinklerZone3.getZoneNumber(), sprinklerZone3)
        copySprinklerZoneMap.put(sprinklerZone3.getZoneNumber(), sprinklerZone3)

        zoneRepository.sprinklerZoneHashMap = sprinklerZoneMap

        zonesService.zoneRepository = zoneRepository
    }

    def "returns all sprinkler zones"(){
        given:
        Collection<SprinklerZone> sprinklerZoneCollection = copySprinklerZoneMap.values()
        List<SprinklerZone> sprinklerZoneList = new ArrayList<>()
        sprinklerZoneList.addAll(sprinklerZoneCollection)

        expect:
        sprinklerZoneList == zonesService.retrieveAllZones()
    }

    def "adds new sprinkler zone"(){
        given:
        SprinklerZone sprinklerZone = new SprinklerZone()
        sprinklerZone.zoneName = "Oak tree"
        sprinklerZone.zoneNumber = "7"
        zonesService.addZone(sprinklerZone)

        copySprinklerZoneMap.put(sprinklerZone.zoneNumber, sprinklerZone)
        Collection<SprinklerZone> sprinklerZoneCollection = copySprinklerZoneMap.values()
        List<SprinklerZone> sprinklerZoneList = new ArrayList<>()
        sprinklerZoneList.addAll(sprinklerZoneCollection)

        expect:
        sprinklerZoneList == zonesService.retrieveAllZones()
    }

    def "completes zone list"(){
        given:
        List<SprinklerZone> incompleteZoneList = new ArrayList<>()
        SprinklerZone sprinklerZone1 = new SprinklerZone()
        sprinklerZone1.setZoneNumber(1)
        incompleteZoneList.add(sprinklerZone1)
        SprinklerZone sprinklerZone2 = new SprinklerZone()
        sprinklerZone2.setZoneNumber(2)
        incompleteZoneList.add(sprinklerZone2)
        SprinklerZone sprinklerZone3 = new SprinklerZone()
        sprinklerZone3.setZoneNumber(3)
        incompleteZoneList.add(sprinklerZone3)
        List<SprinklerZone> sprinklerZoneList = new ArrayList<>()
        sprinklerZoneList= zonesService.retrieveAllZones()

        expect:
        sprinklerZoneList == zonesService.completeZoneList(incompleteZoneList)
    }

    def "deletes zone"(){
        given:
        SprinklerZone sprinklerZone = new SprinklerZone()
        sprinklerZone.setZoneNumber(1)
        sprinklerZone.setZoneName("East flower bed")
        copySprinklerZoneMap.remove(sprinklerZone.zoneNumber)
        Collection<SprinklerZone> sprinklerZoneCollection = copySprinklerZoneMap.values()
        List<SprinklerZone> sprinklerZoneList = new ArrayList<>()
        sprinklerZoneList.addAll(sprinklerZoneCollection)

        expect:
        zonesService.deleteZone(sprinklerZone)
        sprinklerZoneList == zonesService.retrieveAllZones()
    }
}
