import com.laurenrawrface.freebird.service.SprinklerAgendaService
import com.laurenrawrface.freebird.service.ThisDayAgendaService
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit
import com.laurenrawrface.freebird.sprinklers.SprinklerZone
import org.joda.time.LocalTime
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import static org.mockito.Mockito.when
import spock.lang.Specification

import java.time.DayOfWeek

class ThisDayAgendaServiceTest extends Specification{
    ThisDayAgendaService thisDayAgendaService = new ThisDayAgendaService()
    List<SprinklerCircuit> mondayCircuitList = new ArrayList<>()

    @Mock
    SprinklerAgendaService sprinklerAgendaService;

    def setup(){
        Integer circuitID = 1

        List <SprinklerCircuit> sprinklerCircuitList = new ArrayList<>()
        SprinklerCircuit sprinklerCircuit1 = new SprinklerCircuit()
        SprinklerCircuit sprinklerCircuit2 = new SprinklerCircuit()
        SprinklerCircuit sprinklerCircuit3 = new SprinklerCircuit()

        sprinklerCircuit1.circuitID = circuitID
        circuitID++

        sprinklerCircuit2.circuitID = circuitID
        circuitID++

        sprinklerCircuit3.circuitID = circuitID
        circuitID++

        SprinklerZone sprinklerZone1 = new SprinklerZone()
        sprinklerZone1.zoneNumber = 1
        sprinklerZone1.zoneName = "East flower bed"
        SprinklerZone sprinklerZone2 = new SprinklerZone()
        sprinklerZone2.zoneNumber = 2
        sprinklerZone2.zoneName = "Front north lawn"
        SprinklerZone sprinklerZone3 = new SprinklerZone()
        sprinklerZone3.zoneNumber = 3
        sprinklerZone3.zoneName = "Back south lawn"

        List<SprinklerZone> zones1 = new ArrayList<>()
        zones1.add(sprinklerZone1)
        zones1.add(sprinklerZone3)
        sprinklerCircuit1.zones = zones1

        List<SprinklerZone> zones2 = new ArrayList<>()
        zones2.add(sprinklerZone2)
        sprinklerCircuit2.zones = zones2

        List<SprinklerZone> zones3 = new ArrayList<>()
        zones3.add(sprinklerZone1)
        zones3.add(sprinklerZone2)
        sprinklerCircuit3.zones = zones3

        LocalTime startTime1 = new LocalTime(9, 0)
        sprinklerCircuit1.startTime = startTime1

        LocalTime startTime2 = new LocalTime(19, 20)
        sprinklerCircuit2.startTime = startTime2

        LocalTime startTime3 = new LocalTime(6, 10)
        sprinklerCircuit3.startTime = startTime3

        sprinklerCircuit1.durationInMinutes = 15
        sprinklerCircuit2.durationInMinutes = 20

        sprinklerCircuit3.durationInMinutes = 5

        List<DayOfWeek> daysOfWeek1 = new ArrayList<>()
        daysOfWeek1.add(DayOfWeek.MONDAY)
        daysOfWeek1.add(DayOfWeek.WEDNESDAY)
        daysOfWeek1.add(DayOfWeek.FRIDAY)
        sprinklerCircuit1.daysOfWeek = daysOfWeek1

        List<DayOfWeek> daysOfWeek2 = new ArrayList<>()
        daysOfWeek2.add(DayOfWeek.TUESDAY)
        daysOfWeek2.add(DayOfWeek.THURSDAY)
        daysOfWeek2.add(DayOfWeek.SATURDAY)
        sprinklerCircuit2.daysOfWeek = daysOfWeek2

        List<DayOfWeek> daysOfWeek3 = new ArrayList<>()
        daysOfWeek3.add(DayOfWeek.THURSDAY)
        daysOfWeek3.add(DayOfWeek.FRIDAY)
        daysOfWeek3.add(DayOfWeek.SUNDAY)
        sprinklerCircuit3.daysOfWeek = daysOfWeek3

        sprinklerCircuitList.add(sprinklerCircuit1)
        sprinklerCircuitList.add(sprinklerCircuit2)
        sprinklerCircuitList.add(sprinklerCircuit3)

        MockitoAnnotations.initMocks(this)
        when(sprinklerAgendaService.retrieveAgenda()).thenReturn(sprinklerCircuitList)

        thisDayAgendaService.sprinklerAgendaService =sprinklerAgendaService

        mondayCircuitList.add(sprinklerCircuit1)
    }

    def "returns single day agenda"(){
        given:
        Integer dayOfWeek = 1
        List<SprinklerCircuit> mondaySprinklerCircuit = thisDayAgendaService.retrieveAgendaForDay(dayOfWeek)

        expect:
        mondayCircuitList == mondaySprinklerCircuit
    }
}
