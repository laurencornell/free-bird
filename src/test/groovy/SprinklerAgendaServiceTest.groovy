import com.laurenrawrface.freebird.repository.SprinklerAgendaRepository
import com.laurenrawrface.freebird.service.SprinklerAgendaService
import com.laurenrawrface.freebird.service.ZonesService
import com.laurenrawrface.freebird.sprinklers.forms.DeleteSprinklerCircuitForm
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit
import com.laurenrawrface.freebird.sprinklers.SprinklerZone
import org.joda.time.LocalTime
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import static org.mockito.Mockito.when

import spock.lang.Specification

import java.time.DayOfWeek

class SprinklerAgendaServiceTest extends Specification{

    SprinklerAgendaRepository sprinklerAgendaRepository = new SprinklerAgendaRepository()
    SprinklerAgendaService sprinklerAgendaService = new SprinklerAgendaService()
    List<SprinklerCircuit> sprinklerCircuitList = new ArrayList<>()
    List<SprinklerCircuit> copySprinklerCircuitList = new ArrayList<>()
    List<SprinklerZone> incompleteZoneList = new ArrayList<>()
    Integer circuitID

    @Mock
    ZonesService zonesService

    def setup(){
        incompleteZoneList.add(new SprinklerZone(zoneNumber: 1))

        List<SprinklerZone> completeZoneList = new ArrayList<>()
        completeZoneList.add(new SprinklerZone(zoneNumber: 1, zoneName: "East flower bed"))

        MockitoAnnotations.initMocks(this)
        when(zonesService.completeZoneList(incompleteZoneList)).thenReturn(completeZoneList)
        sprinklerAgendaService.zonesService = zonesService

        circuitID = 1

        SprinklerCircuit sprinklerCircuit1 = new SprinklerCircuit()
        SprinklerCircuit sprinklerCircuit2 = new SprinklerCircuit()

        sprinklerCircuit1.setCircuitID(circuitID)
        circuitID++

        sprinklerCircuit2.setCircuitID(circuitID)
        circuitID++
        sprinklerAgendaRepository.circuitID = circuitID

        List<SprinklerZone> zones1 = new ArrayList<>()
        SprinklerZone sprinklerZone1 = new SprinklerZone()
        sprinklerZone1.setZoneNumber(1)
        sprinklerZone1.setZoneName("East flower bed")
        zones1.add(sprinklerZone1)
        SprinklerZone sprinklerZone3 = new SprinklerZone()
        sprinklerZone3.setZoneNumber(3)
        sprinklerZone3.setZoneName("Back south lawn")
        zones1.add(sprinklerZone3)
        sprinklerCircuit1.setZones(zones1)

        List<SprinklerZone> zones2 = new ArrayList<>()
        SprinklerZone sprinklerZone2 = new SprinklerZone()
        sprinklerZone2.setZoneNumber(2)
        sprinklerZone2.setZoneName("Front north lawn")
        zones2.add(sprinklerZone2)
        sprinklerCircuit2.setZones(zones2)

        LocalTime startTime1 = new LocalTime(9, 0)
        sprinklerCircuit1.setStartTime(startTime1)

        LocalTime startTime2 = new LocalTime(19, 20)
        sprinklerCircuit2.setStartTime(startTime2)

        sprinklerCircuit1.setDurationInMinutes(15)
        sprinklerCircuit2.setDurationInMinutes(20)

        List<DayOfWeek> daysOfWeek1 = new ArrayList<>()
        daysOfWeek1.add(DayOfWeek.MONDAY)
        daysOfWeek1.add(DayOfWeek.WEDNESDAY)
        daysOfWeek1.add(DayOfWeek.FRIDAY)
        sprinklerCircuit1.setDaysOfWeek(daysOfWeek1)

        List<DayOfWeek> daysOfWeek2 = new ArrayList<>()
        daysOfWeek2.add(DayOfWeek.TUESDAY)
        daysOfWeek2.add(DayOfWeek.THURSDAY)
        daysOfWeek2.add(DayOfWeek.SATURDAY)
        sprinklerCircuit2.setDaysOfWeek(daysOfWeek2)

        sprinklerCircuitList.add(sprinklerCircuit1)
        sprinklerCircuitList.add(sprinklerCircuit2)

        copySprinklerCircuitList.add(sprinklerCircuit1)
        copySprinklerCircuitList.add(sprinklerCircuit2)

        sprinklerAgendaRepository.sprinklerCircuitList = sprinklerCircuitList

        sprinklerAgendaService.sprinklerAgendaRepository = sprinklerAgendaRepository
    }

    def "retrieve entire agenda"(){
        expect:
        copySprinklerCircuitList == sprinklerAgendaService.retrieveAgenda()
    }

    def "adds new circuit"(){
        given:
        SprinklerCircuit sprinklerCircuit = new SprinklerCircuit()
        sprinklerCircuit.zones = incompleteZoneList
        sprinklerCircuit.startTime = LocalTime.now()
        sprinklerCircuit.durationInMinutes = 10
        List<DayOfWeek> dayOfWeekArrayList = new ArrayList<>()
        dayOfWeekArrayList.add(DayOfWeek.SATURDAY)
        sprinklerCircuit.daysOfWeek = dayOfWeekArrayList

        expect:
        sprinklerAgendaService.addNewCircuit(sprinklerCircuit)
        copySprinklerCircuitList.add(sprinklerAgendaService.completeSprinklerCircuit)
        copySprinklerCircuitList == sprinklerAgendaService.retrieveAgenda()
    }

    def "deletes circuit"(){
        given:
        SprinklerCircuit sprinklerCircuit = sprinklerCircuitList.get(1)
        copySprinklerCircuitList.remove(sprinklerCircuit)
        DeleteSprinklerCircuitForm deleteSprinklerCircuitForm = new DeleteSprinklerCircuitForm()
        deleteSprinklerCircuitForm.circuitID = sprinklerCircuit.circuitID

        expect:
        sprinklerAgendaService.deleteCircuit(deleteSprinklerCircuitForm)
        copySprinklerCircuitList == sprinklerAgendaService.retrieveAgenda()
    }
}
