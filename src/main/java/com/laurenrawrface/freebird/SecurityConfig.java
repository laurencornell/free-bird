//package com.laurenrawrface.freebird;
//
//import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.context.annotation.Configuration;
////import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
////import org.springframework.security.config.annotation.web.builders.HttpSecurity;
////import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
////import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
////import org.springframework.security.config.authentication.AuthenticationManagerBeanDefinitionParser;
////
////@Configuration
////@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter{
//    @Override
//    protected void configure(HttpSecurity http) throws Exception{
//        http
//                .authorizeRequests()
//                    .antMatchers("/").permitAll() //anyone can access "/" page
//                    .anyRequest().authenticated()
//                    .and()
//                .formLogin()
//                    .loginPage("/signIn")
//                    .permitAll()    //anyone can access login page
//                    .and()
//                .logout()
//                    .permitAll();
//    }
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
//        auth
//                .inMemoryAuthentication()
//                    .withUser("user").password("password").roles("USER");
//    }
//}
