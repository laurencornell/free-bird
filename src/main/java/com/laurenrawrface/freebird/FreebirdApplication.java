package com.laurenrawrface.freebird;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreebirdApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreebirdApplication.class, args);
	}
}
