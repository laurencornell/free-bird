package com.laurenrawrface.freebird.sprinklers.forms;

public class DeleteSprinklerCircuitForm {
    public Integer getCircuitID() {
        return circuitID;
    }

    public void setCircuitID(Integer circuitID) {
        this.circuitID = circuitID;
    }

    private Integer circuitID;
}
