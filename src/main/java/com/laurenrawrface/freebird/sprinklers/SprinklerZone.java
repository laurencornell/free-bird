package com.laurenrawrface.freebird.sprinklers;

import java.util.TreeMap;

public class SprinklerZone {
    Integer zoneNumber;
    String zoneName;

    public Integer getZoneNumber() {
        return zoneNumber;
    }

    public void setZoneNumber(Integer zoneNumber) {
        this.zoneNumber = zoneNumber;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }
}
