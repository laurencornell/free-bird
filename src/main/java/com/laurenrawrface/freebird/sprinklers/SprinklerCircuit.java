package com.laurenrawrface.freebird.sprinklers;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.time.DayOfWeek;
import java.util.List;

public class SprinklerCircuit {
    private Integer circuitID;
    private LocalTime startTime;
    private Integer durationInMinutes;
    private List<SprinklerZone> zones;
    private List<DayOfWeek> dayaOfWeek;

    public List<DayOfWeek> getDaysOfWeek() {
        return dayaOfWeek;
    }

    public Integer getCircuitID() {
        return circuitID;
    }

    public void setCircuitID(Integer circuitID) {
        this.circuitID = circuitID;
    }
    public void setDaysOfWeek(List<DayOfWeek> daysOfWeek) {
        this.dayaOfWeek = daysOfWeek;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public List<SprinklerZone> getZones() {
        return zones;
    }

    public void setZones(List<SprinklerZone> zones) {
        this.zones = zones;
    }

}
