package com.laurenrawrface.freebird.mvc.signIn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SignInPageController {
    @RequestMapping("/signIn")
    public ModelAndView signInController()
    {
        return new ModelAndView("signIn");
    }

}
