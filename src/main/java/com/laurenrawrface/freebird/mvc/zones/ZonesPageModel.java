package com.laurenrawrface.freebird.mvc.zones;

import com.laurenrawrface.freebird.sprinklers.SprinklerZone;

import java.util.ArrayList;
import java.util.List;

public class ZonesPageModel {
    private List<SprinklerZone> sprinklerZoneList;

    public List<SprinklerZone> getSprinklerZoneList() {
        return sprinklerZoneList;
    }

    public void setSprinklerZoneList(List<SprinklerZone> sprinklerZoneList) {
        this.sprinklerZoneList = sprinklerZoneList;
    }
}
