package com.laurenrawrface.freebird.mvc.zones;

import com.laurenrawrface.freebird.service.ZonesService;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/zones")
public class ZonesController {
    @Autowired
    private ZonesService zonesService;

    @RequestMapping(value = "")
    public ModelAndView zonesPage() {
    ZonesPageModel zonesPageModel = new ZonesPageModel();

    List<SprinklerZone> sprinklerZoneList=zonesService.retrieveAllZones();

    zonesPageModel.setSprinklerZoneList(sprinklerZoneList);

    return new ModelAndView("zones", "model", zonesPageModel);
}

    @RequestMapping(value = "/newZone", method = RequestMethod.POST)
    public ModelAndView newZoneForm(@ModelAttribute SprinklerZone sprinklerZone){
    ZonesPageModel zonesPageModel = new ZonesPageModel();

    zonesService.addZone(sprinklerZone);

    List<SprinklerZone> sprinklerZoneList=zonesService.retrieveAllZones();

    zonesPageModel.setSprinklerZoneList(sprinklerZoneList);

    return new ModelAndView("zones", "model", zonesPageModel);
}

    @RequestMapping(value="/deleteZone", method = RequestMethod.POST)
    public String deleteZone(@ModelAttribute SprinklerZone sprinklerZone){
        zonesService.deleteZone(sprinklerZone);

        return "redirect:/zones";
    }

}
