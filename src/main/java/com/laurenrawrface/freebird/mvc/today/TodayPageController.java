package com.laurenrawrface.freebird.mvc.today;

import com.laurenrawrface.freebird.service.ThisDayAgendaService;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Controller
public class TodayPageController {
    @Autowired
    private ThisDayAgendaService thisDayAgendaService;

    @RequestMapping("/today")
    public ModelAndView todayPageController() {
        DayOfWeek today=LocalDate.now().getDayOfWeek();
        Integer todayInteger = today.getValue();

        List<SprinklerCircuit> sprinklerCircuitList=thisDayAgendaService.retrieveAgendaForDay(todayInteger);

        TodayPageModel todayPageModel = new TodayPageModel();

        todayPageModel.setSprinklerCircuitArrayList(sprinklerCircuitList);

        return new ModelAndView("today", "model", todayPageModel);
    }
}

