package com.laurenrawrface.freebird.mvc.today;

import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;

import java.util.List;

public class TodayPageModel {
    public List<SprinklerCircuit> getSprinklerCircuitArrayList() {
        return sprinklerCircuitArrayList;
    }

    public void setSprinklerCircuitArrayList(List<SprinklerCircuit> sprinklerCircuitArrayList) {
        this.sprinklerCircuitArrayList = sprinklerCircuitArrayList;
    }

    private List<SprinklerCircuit> sprinklerCircuitArrayList;

}
