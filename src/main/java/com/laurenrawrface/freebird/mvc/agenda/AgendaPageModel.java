package com.laurenrawrface.freebird.mvc.agenda;

import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;

import java.util.ArrayList;
import java.util.List;

public class AgendaPageModel {
    private List<SprinklerCircuit> sprinklerCircuitArrayList;
    private List<SprinklerZone> sprinklerZoneArrayList;

    public List<SprinklerZone> getSprinklerZoneArrayList() {
        return sprinklerZoneArrayList;
    }

    public void setSprinklerZoneArrayList(List<SprinklerZone> sprinklerZoneArrayList) {
        this.sprinklerZoneArrayList = sprinklerZoneArrayList;
    }

    public List<SprinklerCircuit> getSprinklerCircuitArrayList() {
        return sprinklerCircuitArrayList;
    }

    public void setSprinklerCircuitArrayList(List<SprinklerCircuit> sprinklerCircuitArrayList) {
        this.sprinklerCircuitArrayList = sprinklerCircuitArrayList;
    }
}
