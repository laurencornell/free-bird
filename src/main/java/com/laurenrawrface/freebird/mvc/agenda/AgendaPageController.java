package com.laurenrawrface.freebird.mvc.agenda;

import com.laurenrawrface.freebird.service.SprinklerAgendaService;
import com.laurenrawrface.freebird.service.ZonesService;
import com.laurenrawrface.freebird.sprinklers.forms.DeleteSprinklerCircuitForm;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import com.laurenrawrface.freebird.sprinklers.forms.SprinklerCircuitForm;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.DayOfWeek;
import java.util.*;

/**Agenda page contains all scheduled waterings throughout the week
 *
 */

@Controller
@RequestMapping("/agenda")
public class AgendaPageController {
    @Autowired
    private SprinklerAgendaService sprinklerAgendaService;

    @Autowired
    private ZonesService zonesService;

    @RequestMapping("")
    public ModelAndView agendaPage()
    {
        List<SprinklerZone> sprinklerZoneList = zonesService.retrieveAllZones();

        List<SprinklerCircuit> sprinklerCircuitList = sprinklerAgendaService.retrieveAgenda();

        AgendaPageModel agendaPageModel=new AgendaPageModel();
        agendaPageModel.setSprinklerCircuitArrayList(sprinklerCircuitList);
        agendaPageModel.setSprinklerZoneArrayList(sprinklerZoneList);

        return new ModelAndView("agenda", "model", agendaPageModel);
    }

    @RequestMapping(value="/newCircuit", method= RequestMethod.POST)
    public String newCircuitAgendaPage(@ModelAttribute SprinklerCircuitForm sprinklerCircuitForm)
    {
        AgendaPageModel agendaPageModel = new AgendaPageModel();

        SprinklerCircuit sprinklerCircuit = new SprinklerCircuit();

        sprinklerCircuit.setDurationInMinutes(sprinklerCircuitForm.getDurationInMinutes());

        List<String> zoneStringList = Arrays.asList(sprinklerCircuitForm.getZone().split(","));
        List<Integer> zoneIntegerList = new ArrayList<>();
        for (String zone: zoneStringList){
            zoneIntegerList.add(Integer.valueOf(zone));
        }
        List<SprinklerZone> sprinklerZoneNumberList = new ArrayList<>();
        for (Integer zoneNumber: zoneIntegerList){
            SprinklerZone sprinklerZone = new SprinklerZone();
            sprinklerZone.setZoneNumber(zoneNumber);
            sprinklerZoneNumberList.add(sprinklerZone);
        }
        sprinklerCircuit.setZones(sprinklerZoneNumberList);

        List<String> daysList= Arrays.asList(sprinklerCircuitForm.getDays().split(","));
        List<DayOfWeek> dayOfWeekList = new ArrayList<>();
        for (String day : daysList){
            DayOfWeek dayOfWeek = DayOfWeek.valueOf(day.toUpperCase());
            dayOfWeekList.add(dayOfWeek);
        }
        sprinklerCircuit.setDaysOfWeek(dayOfWeekList);

        LocalTime startTime = new LocalTime(sprinklerCircuitForm.getHour(), sprinklerCircuitForm.getMinutes());
        sprinklerCircuit.setStartTime(startTime);
//
//        List<DateTime> startTimeList = sprinklerAgendaService.retrieveStartTimeList(startTime, dayOfWeekList);
//        sprinklerCircuit.setStartTimeList(startTimeList);

        sprinklerAgendaService.addNewCircuit(sprinklerCircuit);

        List<SprinklerZone> sprinklerZoneList = zonesService.retrieveAllZones();

        List<SprinklerCircuit> sprinklerCircuitList = sprinklerAgendaService.retrieveAgenda();

        agendaPageModel.setSprinklerCircuitArrayList(sprinklerCircuitList);
        agendaPageModel.setSprinklerZoneArrayList(sprinklerZoneList);

        return "redirect:/agenda";
    }

    @RequestMapping(value = "/deleteCircuit", method = RequestMethod.POST)
    public String deleteCircuitFromList(@ModelAttribute DeleteSprinklerCircuitForm deleteSprinklerCircuitForm){
        sprinklerAgendaService.deleteCircuit(deleteSprinklerCircuitForm);

        return "redirect:/agenda";
    }
}
