package com.laurenrawrface.freebird.mvc.home;

import java.time.LocalDateTime;

public class HomePageModel {
    public LocalDateTime getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(LocalDateTime currentTime) {
        this.currentTime = currentTime;
    }

    private LocalDateTime currentTime;
}
