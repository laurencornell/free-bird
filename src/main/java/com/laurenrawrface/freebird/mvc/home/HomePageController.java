package com.laurenrawrface.freebird.mvc.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Controller
public class HomePageController {
    @RequestMapping("/")
    public ModelAndView homePage()
    {
        LocalDateTime currentTime = LocalDateTime.now();

        HomePageModel homePageModel = new HomePageModel();

        homePageModel.setCurrentTime(currentTime);

        return new ModelAndView("home", "homePageModel", homePageModel);
    }

}
