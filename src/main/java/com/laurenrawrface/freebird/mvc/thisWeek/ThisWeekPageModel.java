package com.laurenrawrface.freebird.mvc.thisWeek;

import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;

import java.util.ArrayList;
import java.util.List;

public class ThisWeekPageModel {
    private List<SprinklerCircuit> sprinklerCircuitArrayList;
    private String dayOfWeek;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public List<SprinklerCircuit> getSprinklerCircuitArrayList() {
        return sprinklerCircuitArrayList;
    }

    public void setSprinklerCircuitArrayList(List<SprinklerCircuit> sprinklerCircuitArrayList) {
        this.sprinklerCircuitArrayList = sprinklerCircuitArrayList;
    }
}
