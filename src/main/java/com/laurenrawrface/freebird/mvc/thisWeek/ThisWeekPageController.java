package com.laurenrawrface.freebird.mvc.thisWeek;

import com.laurenrawrface.freebird.service.SprinklerAgendaService;
import com.laurenrawrface.freebird.service.ThisDayAgendaService;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Controller
public class ThisWeekPageController {

    @Autowired
    private ThisDayAgendaService thisDayAgendaService;

    @RequestMapping("/thisWeek/{dayOfWeek}")
    public ModelAndView thisWeekController(@PathVariable Integer dayOfWeek) {
        ThisWeekPageModel thisWeekPageModel = new ThisWeekPageModel();

        DateTime dayOfWeekDate = new DateTime().withMillisOfDay(0).withDayOfWeek(dayOfWeek);

        String day = dayOfWeekDate.dayOfWeek().getAsText();

        List<SprinklerCircuit> sprinklerCircuitList = thisDayAgendaService.retrieveAgendaForDay(dayOfWeek);

        thisWeekPageModel.setSprinklerCircuitArrayList(sprinklerCircuitList);
        thisWeekPageModel.setDayOfWeek(day);

        return new ModelAndView("thisWeek", "model", thisWeekPageModel);
}
}
