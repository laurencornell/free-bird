package com.laurenrawrface.freebird.service;

import com.laurenrawrface.freebird.repository.ZoneRepository;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ZonesService {
    @Autowired
    private ZoneRepository zoneRepository;

    public List<SprinklerZone> retrieveAllZones(){
        Collection<SprinklerZone> sprinklerZoneCollection = zoneRepository.retrieveAllZones();

        List<SprinklerZone> result= new ArrayList<>(sprinklerZoneCollection.size());

        result.addAll(sprinklerZoneCollection);

        return result;
    }

    public void addZone(SprinklerZone sprinklerZone) {
    zoneRepository.addZone(sprinklerZone);
    }

    public List<SprinklerZone> completeZoneList(List<SprinklerZone> zones) {
        List<SprinklerZone> sprinklerZoneSet = new ArrayList<>();

        for(SprinklerZone zone : zones){
            SprinklerZone sprinklerZone = zoneRepository.getCompleteZone(zone);
            sprinklerZoneSet.add(sprinklerZone);
        }

        return sprinklerZoneSet;
    }

    public void deleteZone(SprinklerZone sprinklerZone) {
    zoneRepository.deleteZone(sprinklerZone);
    }
}
