package com.laurenrawrface.freebird.service;

import com.laurenrawrface.freebird.repository.SprinklerAgendaRepository;
import com.laurenrawrface.freebird.sprinklers.forms.DeleteSprinklerCircuitForm;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.joda.time.DateTimeComparator;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

@Service
public class SprinklerAgendaService {
    private SprinklerCircuit completeSprinklerCircuit;
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SprinklerAgendaRepository sprinklerAgendaRepository;

    @Autowired
    private ZonesService zonesService;

    @Autowired
    private ThisDayAgendaService thisDayAgendaService;

    public List<SprinklerCircuit> retrieveAgenda(){
        Collection<SprinklerCircuit> agendaForDayCollection = sprinklerAgendaRepository.retrieveAgenda();

        List<SprinklerCircuit> result = new ArrayList<>(agendaForDayCollection.size());

        result.addAll(agendaForDayCollection);

        return result;
    }

    public void addNewCircuit(SprinklerCircuit sprinklerCircuit) {
        completeSprinklerCircuit = new SprinklerCircuit();

        List<SprinklerZone> completeZoneList = zonesService.completeZoneList(sprinklerCircuit.getZones());
        completeSprinklerCircuit.setZones(completeZoneList);

        completeSprinklerCircuit.setStartTime(sprinklerCircuit.getStartTime());
        completeSprinklerCircuit.setDaysOfWeek(sprinklerCircuit.getDaysOfWeek());
        completeSprinklerCircuit.setDurationInMinutes(sprinklerCircuit.getDurationInMinutes());

        sprinklerAgendaRepository.addNewCircuit(completeSprinklerCircuit);
    }

    public void deleteCircuit(DeleteSprinklerCircuitForm circuitID) {
        sprinklerAgendaRepository.deleteCircuit(circuitID.getCircuitID());
    }

    public void checkCircuits(DateTime now) {
        DayOfWeek today= LocalDate.now().getDayOfWeek();
        Integer todayInteger = today.getValue();
        List<SprinklerCircuit> circuitsForToday = thisDayAgendaService.retrieveAgendaForDay(todayInteger);

        Map<DateTime, SprinklerCircuit> sprinklerCircuitMap = new HashMap<>();

        for (SprinklerCircuit circuit : circuitsForToday){
            LocalTime startTime = circuit.getStartTime();

            DateTime startDateTime = new DateTime().withTime(startTime).withZone(DateTimeZone.getDefault()).withDate(now.toLocalDate());

            sprinklerCircuitMap.put(startDateTime, circuit);
        }

            for (Map.Entry<DateTime, SprinklerCircuit> entry : sprinklerCircuitMap.entrySet() ){
                DateTime startDateTime = entry.getKey();
                SprinklerCircuit circuit = entry.getValue();

                Integer durationInMinutes = circuit.getDurationInMinutes();
                DateTime endTime = startDateTime.plusMinutes(durationInMinutes);

                    if (DateTimeComparator.getTimeOnlyInstance().compare(now.withSecondOfMinute(0).withMillisOfSecond(0), startDateTime) == 0){
                        LOGGER.info("Starting circuit at " + now.toString());

                        //start sprinkler
                    }

                    else if (DateTimeComparator.getTimeOnlyInstance().compare(now.withSecondOfMinute(0).withMillisOfSecond(0), endTime) == 0){
                        LOGGER.info("Turning off sprinkler at " + now.toString());
                    }
                }
    }
}
