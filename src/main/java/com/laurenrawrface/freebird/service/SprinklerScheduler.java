package com.laurenrawrface.freebird.service;

import com.laurenrawrface.freebird.service.SprinklerAgendaService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@EnableScheduling
@Service
public class SprinklerScheduler {
    @Autowired
    private SprinklerAgendaService sprinklerAgendaService = new SprinklerAgendaService();

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Scheduled(fixedRate = 60 * 1000)
    public void checkCircuits(){
        DateTime now = new DateTime(DateTimeZone.UTC);

        LOGGER.info("Checking circuits at " + now.withZone(DateTimeZone.getDefault()).toString());

        sprinklerAgendaService.checkCircuits(now);
    }
}
