package com.laurenrawrface.freebird.service;

import com.laurenrawrface.freebird.repository.SprinklerAgendaRepository;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@Service
public class ThisDayAgendaService {
    @Autowired
    private SprinklerAgendaService sprinklerAgendaService;

    public List<SprinklerCircuit> retrieveAgendaForDay(Integer dayOfWeekInteger) {
        DateTime dayOfWeekDate = new DateTime().withMillisOfDay(0).withDayOfWeek(dayOfWeekInteger);
        String day = dayOfWeekDate.dayOfWeek().getAsText();
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(day.toUpperCase());

        List<SprinklerCircuit> entireSprinklerAgenda =sprinklerAgendaService.retrieveAgenda();

        List<SprinklerCircuit> agendaForDay = new ArrayList<>();

        for (SprinklerCircuit sprinklerCircuit: entireSprinklerAgenda){
            List<DayOfWeek> dayOfWeekList = sprinklerCircuit.getDaysOfWeek();
            if(dayOfWeekList.contains(dayOfWeek)){
                agendaForDay.add(sprinklerCircuit);
            }
        }

        return agendaForDay;
    }
}
