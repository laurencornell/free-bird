package com.laurenrawrface.freebird.repository;

import com.laurenrawrface.freebird.service.SprinklerAgendaService;
import com.laurenrawrface.freebird.sprinklers.SprinklerCircuit;
import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.DayOfWeek;
import java.util.*;

@Repository
public class SprinklerAgendaRepository {
    private List<SprinklerCircuit> sprinklerCircuitList;
    private Integer circuitID;

    @Autowired
    SprinklerAgendaService sprinklerAgendaService = new SprinklerAgendaService();

    @PostConstruct
    private void setup()
    {
        circuitID=1;

        sprinklerCircuitList = new ArrayList<>();
        SprinklerCircuit sprinklerCircuit1 = new SprinklerCircuit();
        SprinklerCircuit sprinklerCircuit2 = new SprinklerCircuit();
        SprinklerCircuit sprinklerCircuit3 = new SprinklerCircuit();

        sprinklerCircuit1.setCircuitID(circuitID);
        circuitID++;

        sprinklerCircuit2.setCircuitID(circuitID);
        circuitID++;

        sprinklerCircuit3.setCircuitID(circuitID);
        circuitID++;

        List<SprinklerZone> zones1 = new ArrayList<>();
        SprinklerZone sprinklerZone1 = new SprinklerZone();
        sprinklerZone1.setZoneNumber(1);
        sprinklerZone1.setZoneName("East flower bed");
        zones1.add(sprinklerZone1);
        SprinklerZone sprinklerZone3 = new SprinklerZone();
        sprinklerZone3.setZoneNumber(3);
        sprinklerZone3.setZoneName("Back south lawn");
        zones1.add(sprinklerZone3);
        sprinklerCircuit1.setZones(zones1);

        List<SprinklerZone> zones2 = new ArrayList<>();
        SprinklerZone sprinklerZone2 = new SprinklerZone();
        sprinklerZone2.setZoneNumber(2);
        sprinklerZone2.setZoneName("Front north lawn");
        zones2.add(sprinklerZone2);
        sprinklerCircuit2.setZones(zones2);

        List<SprinklerZone> zones3 = new ArrayList<>();
        zones3.add(sprinklerZone1);
        zones3.add(sprinklerZone2);
        sprinklerCircuit3.setZones(zones3);

        LocalTime startTime1 = new LocalTime(9, 0);
        sprinklerCircuit1.setStartTime(startTime1);

        LocalTime startTime2 = new LocalTime(19, 20);
        sprinklerCircuit2.setStartTime(startTime2);

        LocalTime startTime3 = new LocalTime(6, 10);
        sprinklerCircuit3.setStartTime(startTime3);

        sprinklerCircuit1.setDurationInMinutes(15);

        sprinklerCircuit2.setDurationInMinutes(20);

        sprinklerCircuit3.setDurationInMinutes(5);

        List<DayOfWeek> daysOfWeek1 = new ArrayList<>();
        daysOfWeek1.add(DayOfWeek.MONDAY);
        daysOfWeek1.add(DayOfWeek.WEDNESDAY);
        daysOfWeek1.add(DayOfWeek.FRIDAY);
        sprinklerCircuit1.setDaysOfWeek(daysOfWeek1);

        List<DayOfWeek> daysOfWeek2 = new ArrayList<>();
        daysOfWeek2.add(DayOfWeek.TUESDAY);
        daysOfWeek2.add(DayOfWeek.THURSDAY);
        daysOfWeek2.add(DayOfWeek.SATURDAY);
        sprinklerCircuit2.setDaysOfWeek(daysOfWeek2);

        List<DayOfWeek> daysOfWeek3 = new ArrayList<>();
        daysOfWeek3.add(DayOfWeek.THURSDAY);
        daysOfWeek3.add(DayOfWeek.FRIDAY);
        daysOfWeek3.add(DayOfWeek.SUNDAY);
        sprinklerCircuit3.setDaysOfWeek(daysOfWeek3);
//
//        List<DateTime> startTimeList1 = sprinklerAgendaService.retrieveStartTimeList(startTime1, daysOfWeek1);
//        sprinklerCircuit1.setStartTimeList(startTimeList1);
//
//        List<DateTime> startTimeList2 = sprinklerAgendaService.retrieveStartTimeList(startTime2, daysOfWeek2);
//        sprinklerCircuit2.setStartTimeList(startTimeList2);
//
//        List<DateTime> startTimeList3 = sprinklerAgendaService.retrieveStartTimeList(startTime3, daysOfWeek3);
//        sprinklerCircuit3.setStartTimeList(startTimeList3);

        sprinklerCircuitList.add(sprinklerCircuit1);
        sprinklerCircuitList.add(sprinklerCircuit2);
        sprinklerCircuitList.add(sprinklerCircuit3);
    }
    public Collection<SprinklerCircuit> retrieveAgenda() {

        return sprinklerCircuitList;
    }

    public void addNewCircuit(SprinklerCircuit sprinklerCircuit) {
        sprinklerCircuit.setCircuitID(circuitID);
        circuitID++;

        sprinklerCircuitList.add(sprinklerCircuit);
    }

    public void deleteCircuit(Integer circuitID) {
        for (SprinklerCircuit sprinklerCircuit : sprinklerCircuitList){
            if (circuitID==sprinklerCircuit.getCircuitID()){
                sprinklerCircuitList.remove(sprinklerCircuit);
                return;
            }
        }
    }
}
