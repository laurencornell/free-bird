package com.laurenrawrface.freebird.repository;

import com.laurenrawrface.freebird.sprinklers.SprinklerZone;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
public class ZoneRepository {
    private Map<Integer, SprinklerZone> sprinklerZoneHashMap;

    @PostConstruct
    private void setup(){
        sprinklerZoneHashMap = new HashMap<>();

        SprinklerZone sprinklerZone1 = new SprinklerZone();
        SprinklerZone sprinklerZone2 = new SprinklerZone();
        SprinklerZone sprinklerZone3 = new SprinklerZone();

        sprinklerZone1.setZoneNumber(1);
        sprinklerZone1.setZoneName("East flower bed");
        sprinklerZoneHashMap.put(sprinklerZone1.getZoneNumber(), sprinklerZone1);

        sprinklerZone2.setZoneNumber(2);
        sprinklerZone2.setZoneName("Front north lawn");
        sprinklerZoneHashMap.put(sprinklerZone2.getZoneNumber(), sprinklerZone2);

        sprinklerZone3.setZoneNumber(3);
        sprinklerZone3.setZoneName("Back south lawn");
        sprinklerZoneHashMap.put(sprinklerZone3.getZoneNumber(), sprinklerZone3);
    }

    public Collection<SprinklerZone> retrieveAllZones() {
        Collection<SprinklerZone> sprinklerZoneCollection = sprinklerZoneHashMap.values();

        return sprinklerZoneCollection;
    }

    public void addZone(SprinklerZone sprinklerZone) {
    sprinklerZoneHashMap.put(sprinklerZone.getZoneNumber(), sprinklerZone);
    }

    public SprinklerZone getCompleteZone(SprinklerZone zone) {
        SprinklerZone completeZone = sprinklerZoneHashMap.get(zone.getZoneNumber());

        return completeZone;
    }

    public void deleteZone(SprinklerZone sprinklerZone) {
        sprinklerZoneHashMap.remove(sprinklerZone.getZoneNumber());
    }
}
